CURDBEE INTEGRATION
----------------------------------------
This module allows integration with Hiveage invoicing system.

FEATURES
----------------------------------------
Current features include:
1. Assign Hiveage clients to users
2. Allow users to view their invoices by logging into their account

INSTALLATION
----------------------------------------
1. Copy the module as normal.
2. Enable the module from the module administration page.
3. Configure the module (see "Configuration" below).

CONFIGURATION
----------------------------------------
1. Configure Hiveage settings at admin/config/services/hiveage
2. Configure Hiveage permissions at admin/people/permissions
3. Assign Hiveage client to user account by simply creating/editing an account.

CONTACT
----------------------------------------
The current maintainer is Ashish Upadhayay <contact@ashish.com.au>

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue.
